#!/bin/bash
TOPFILE=top.html
BOTFILE=eof.html
OUTFILE=index.html
ARTPATH="./art/*" 
ARTLST=$(ls -t ${ARTPATH})
cat ${TOPFILE} > ${OUTFILE}
for entry in ${ARTLST}
do
  cat ${entry} >> ${OUTFILE}
done
cat ${BOTFILE} >> ${OUTFILE}
